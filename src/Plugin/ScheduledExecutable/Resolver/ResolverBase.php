<?php

namespace Drupal\scheduled_executable\Plugin\ScheduledExecutable\Resolver;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for resolvers.
 */
abstract class ResolverBase extends PluginBase implements ResolverInterface {

}
