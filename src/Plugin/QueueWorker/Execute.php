<?php

namespace Drupal\scheduled_executable\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @QueueWorker(
 *   id = "scheduled_executable_execute",
 *   title = @Translation("Executes scheduled executables"),
 *   cron = {"time" = 15}
 * )
 */
class Execute extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $etmi;

  /**
   * Scheduled executable storage.
   *
   * @var \Drupal\scheduled_executable\Entity\ScheduledExecutable
   */
  protected $scheduledExecutableStorage;

  /**
   * Constructs a new Execute instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $etmi
   *   Entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $etmi) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->etmi = $etmi;
    $this->scheduledExecutableStorage = $this->etmi->getStorage('scheduled_executable');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $scheduled_executable = $this->scheduledExecutableStorage->load($data);

    // Check if scheduled_executable still exists, because it might be removed
    // before queue run.
    if ($scheduled_executable) {
      $plugin = $scheduled_executable->getExecutablePluginInstance();
      $target_entity = $scheduled_executable->getTargetEntity();

      // TODO: catch exception?
      $plugin->execute($target_entity);

      $scheduled_executable->delete();
    }
  }

}
