<?php

namespace Drupal\scheduled_executable\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the resolver plugin annotation object.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class ScheduledExecutableResolver extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin label.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
